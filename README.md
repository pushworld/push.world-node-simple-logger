# node-simple-logger

Класс реализующий простой логгер для node.js с возможность записи логов
в консоль, в файл и в Sentry. 
В основе пакета лежит библиотека [signale.js](https://github.com/klaussinani/signale)

Доступные уровни логирования:
1. debug: 0
2. log: 4
3. info: 4
4. note: 4
5. star: 4
6. fav: 4
7. 'await': 4
8. start: 4
9. pause: 4
10. watch: 4
11. pending: 4
12. success: 4
13. complete: 3
14. warn: 2
15. error: 1
16. fatal: 0

Конструктор класса принимает объект конфигурации:
```
{
    enableConsole?: boolean;
    enableFile?: {
        folderPath: string;
        fileName: string;
        level: string,
        // number of bytes for log file when need to compres
        // if not set - not compress
        compressSize: number
    };
    enableSentry: {
        dsn: string;
    },
    prefix?: string;
}
```
 
## Пример использования пакета
 ```
const Logger = require('simple-logger');
    const log = new Logger({
        enableConsole: true;
        enableSentry: {
            dsn: 'example'
        },
        enableFile: {
            folderPath: '../logs';
            fileName: 'combine.logs';
            level: 'debug',
            compressSize: 20000000 // 20MB
        },
        enableSentry: {
          dsn: "1234"
        },
        prefix: '[API]'
    });
    log.success('Hello, world);
```
