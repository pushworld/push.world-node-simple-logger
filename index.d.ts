// Type definitions for library SimpleLogger
// Project: PushWorld
// Definitions by: TeamDev

/**
 * This declaration specifies that the class constructor function
 * is the exported object from the file
 */
declare class SimpleLogger {
    constructor(options: SimpleLogger.Options);

    debug(arg: any): void;

    log(arg: any): void;

    info(arg: any): void;

    watch(arg: any): void;

    pending(arg: any): void;

    success(arg: any): void;

    complete(arg: any): void;

    fatal(arg: any): void;

    note(arg: any): void;

    star(arg: any): void;

    fav(arg: any): void;

    await(arg: any): void;

    start(arg: any): void;

    pause(arg: any): void;

    warn(arg: any): void;

    error(arg: any): void;

    private _initFileLog(): void;

    private _createLevelLogMethods(): void;

    private _write(level: string, arg: any): void;

    private _writeToConsole(level: string, arg: any): void;

    private _writeToFile(type: string, arg: any): void;

    private _compressLog(): void;
}

export = SimpleLogger;

declare namespace SimpleLogger {
    export interface Options {
        enableConsole?: boolean;
        enableSentry?: {
            dsn: string
        }
        enableFile?: {
            folderPath: string;
            fileName: string;
            level: string,
            compressSize?: number // in MB, if undefined compress log files would not be work
        };
        prefix?: string;
    }
}
