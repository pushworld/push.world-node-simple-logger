/**
 * Simple logger with transports to console and file
 *
 */

// dependencies
const fs = require('fs');
const path = require('path');
const util = require('util');
const signale = require('signale');
const {compresser} = require('push.world-node-utils');
const Sentry = require('@sentry/node');

class SimpleLogger {
    /**
     * @param {SimpleLogger.Options} options
     */
    constructor(options) {
        this._options = options;
        this._setDefaultOptions();

        this.levels = {
            debug: 0,
            log: 4,
            info: 4,
            note: 4,
            star: 4,
            fav: 4,
            'await': 4,
            start: 4,
            pause: 4,
            watch: 4,
            pending: 4,
            success: 4,
            complete: 3,
            warn: 2,
            error: 1,
            fatal: 0
        };

        this._createLevelLogMethods();

        if (this._options.enableSentry && this._options.enableSentry.dsn) {
            Sentry.init({dsn: this._options.enableSentry.dsn});

            /**
             * @private
             */
            this._Sentry = Sentry;
        }

        this._initFileLog();
    }

    /**
     * set default _options
     * @private
     */
    _setDefaultOptions() {
        if (this._options.enableConsole === undefined) {
            this._options.enableConsole = true;
        }
        if (this._options.prefix === undefined) {
            this._options.prefix = '';
        }
    }

    /**
     * check enable log to file
     * if true create file stream to write logs
     * @private
     */
    _initFileLog() {
        if (!this._options.enableFile) {
            return;
        }

        const folderPath = this._options.enableFile.folderPath;
        const filePath = path.resolve(folderPath, this._options.enableFile.fileName);

        if (!fs.existsSync(folderPath)) {
            fs.mkdirSync(folderPath);
        }

        this.fileStream = fs.createWriteStream(filePath, {flags: 'a'});
        this.logLevel = this.levels[this._options.enableFile.level] !== undefined ?
            this.levels[this._options.enableFile.level] : this.levels.debug;

        // init logs size in MB
        this._logsSize = 0;

        process.on('exit', () => {
            this.fileStream.close();
        });
    }

    /**
     * generate log methods according in log levels
     * @private
     */
    _createLevelLogMethods() {
        Object.keys(this.levels).forEach((level) => {
            this[level] = (arg) => {
                this._write(level, arg)
            }
        });
    }

    /**
     * @param {string} level
     * @param {*} arg
     * @private
     */
    _write(level, arg) {
        if (this._options.enableConsole) {
            this._writeToConsole(level, arg);
        }
        if (this._Sentry && (level === 'fatal' || level === 'error')) {
            this._writeToSentry(arg);
        }
        if (this.fileStream && this.levels[level] <= this.logLevel) {
            this._writeToFile(level, arg);
        }
    }

    /**
     * @param {string} level
     * @param {*} arg
     * @private
     */
    _writeToConsole(level, arg) {
        signale[level](new Date().toLocaleString('ru'), this._options.prefix, arg);
    }

    /**
     * @param {string} type
     * @param {*} arg
     * @private
     */
    _writeToFile(type, arg) {
        const strToLog = `[${type}] ${new Date().toLocaleString('ru')}: ${this._options.prefix} ${util.format(arg)}\n`;

        // watch compress size and compress them
        this._logsSize += Buffer.byteLength(strToLog, 'utf8');

        // write log to file
        this.fileStream.write(strToLog);

        // check and if compress and truncate log if needed
        this._compressLog();
    }

    _writeToSentry(arg) {
        if (arg instanceof Error) {
            this._Sentry.captureException(arg);
        } else {
            this._Sentry.captureMessage(util.format(arg));
        }
    }

    /**
     * compress and truncate log file
     * @private
     */
    _compressLog() {
        const {folderPath, fileName, compressSize} = this._options.enableFile;

        // compressing logs
        if (this._logsSize >= compressSize) {

            const sourceFile = fileName;
            const destinationFile = fileName.replace(path.extname(fileName), '') + '-' + Date.now() + '.gz.b64';

            compresser.compress(folderPath, sourceFile, destinationFile, (err) => {
                if (!err) {
                    compresser.truncate(folderPath, sourceFile, (err) => {
                        if (!err) {
                            console.log(`success compressing and truncating log file`);
                            this._logsSize = 0;
                        } else {
                            console.log(`error truncating log file ${sourceFile}: ${err}`);
                        }
                    });
                } else {
                    console.log(`error compressing log file ${sourceFile}:`, err);
                }
            });
        }
    }
}

module.exports = SimpleLogger;
